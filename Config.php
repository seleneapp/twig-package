<?php

/**
 * This File is part of the Selene\Package\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig;

use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\Package\PackageConfiguration;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Loader\XmlLoader;
use \Selene\Module\Config\Resource\Locator;
use \Selene\Module\Config\Validator\Nodes\RootNode;

/**
 * @class Config
 * @package Selene\Package\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Config extends PackageConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function setup(BuilderInterface $builder, array $config)
    {
        $this->setParameter('twig.options', $config['options']);

        $this->setParameter('twig.extensions_default', $config['extensions']['enabled']);
    }

    protected function getResources()
    {
        return ['services.xml'];
    }

    protected function getDefaultExtensionKeys()
    {
        return ['selene_di_parameters', 'array', 'date', 'i18n', 'intl', 'text'];
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTree(RootNode $rootNode)
    {
        $alias = $this->getPackageAlias();
        $rootNode
            ->dict('options')
                ->notEmpty()
                    ->boolean('debug')
                    ->condition()
                        ->ifString()
                        ->then(function ($value) {
                            return (bool)$this->resolveParameter($value);
                        })
                        ->end()
                    ->end()
                    ->string('cache')
                        ->optional()
                            ->condition()
                                ->ifNull()
                                ->then(function ($value) {
                                    return '%app.cache%/twig';
                                })
                            ->end()
                        ->end()
                    ->boolean('auto_reload')
                        ->end()
                ->end()
            ->dict('extensions')->optional()
                ->boolean('all')->defaultValue(true)->optional()
                    ->end()
                ->values('enabled')->defaultValue($extKeys = $this->getDefaultExtensionKeys())->optional()
                    ->string()
                        ->condition()
                            ->ifNotInArray($extKeys)
                            ->thenMarkInvalid()
                            ->end()
                        ->end()
                    ->end()
                ->end();
    }
}
