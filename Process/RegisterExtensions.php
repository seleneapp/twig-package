<?php

/**
 * This File is part of the Selene\Package\Twig\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig\Process;

use \Selene\Module\DI\Reference;
use \Selene\Module\DI\Definition\FlagInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * @class RegisterExtensions
 * @package Selene\Package\Twig\Process
 * @version $Id$
 */
class RegisterExtensions implements ProcessInterface
{
    private $container;

    public function process(ContainerInterface $container)
    {
        if (!$container->hasDefinition('twig.env')) {
            return;
        }

        $this->container = $container;
        $twig = $this->container->getDefinition('twig.env');

        foreach ($container->findDefinitionsWithMetaData($tag = 'twig.extension') as $id => $definition) {
            $twig->addSetter('addExtension', [new Reference($id)]);
        }
    }
}
