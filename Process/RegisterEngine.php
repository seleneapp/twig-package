<?php

/**
 * This File is part of the Selene\Package\Twig\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig\Process;

use \Selene\Module\DI\Reference;
use \Selene\Module\DI\Definition\FlagInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * @class SubscribeMiddlewares
 * @package Selene\Package\Framework\Process
 * @version $Id$
 */
class RegisterEngine implements ProcessInterface
{
    /**
     * process
     *
     * @param ContainerInterface $container
     *
     * @access public
     * @return mixed
     */
    public function process(ContainerInterface $container)
    {
        $this->container = $container;

        if (!$view = $container->getDefinition('view')) {
            return;
        }

        $engines = (array)$view->getArgument(0);
        $engines[] = new Reference('twig.engine');

        $view->replaceArgument($engines, 0);
    }
}
