<?php

/**
 * This File is part of the Selene\Package\Twig\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Adapter\Twig\Process\PrepareExtensions as PrepExt;

/**
 * @class PrepareExtensions
 * @package Selene\Package\Twig\Process
 * @version $Id$
 */
class PrepareExtensions extends PrepExt
{
    public function __construct($tag = null, $extensions = self::EXT_ALL)
    {
        $this->setContainerId('app.container');
        parent::__construct('twig.extension', []);
    }

    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        $this->setExtensions(
            $dd = $container->getParameters()->get('twig.extensions_default')
        );

        $container->getParameters()->remove('twig.extensions_default');

        parent::process($container);
    }
}
